/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 12:04:46 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/08 16:19:37 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_CLASS_HPP
# define FIXED_CLASS_HPP

#include <iostream>

class Fixed {

public:

				Fixed(void);
				Fixed(Fixed const & src);
				Fixed(int const i);
				Fixed(float const f);
				~Fixed(void);

	Fixed&		operator=(Fixed const & rhs);

	int			getRawBits(void) const;
	void		setRawBits(int const raw);
	float		toFloat(void) const;
	int			toInt(void) const;

private:

	int					_fixed_point;
	static const int	_fract_bits;

};

std::ostream&	operator<<(std::ostream & o, Fixed const & i);

#endif /* !FIXED_CLASS_HPP */
