/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 12:04:58 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/08 16:17:56 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.class.hpp"
#include <iostream>
#include <cmath>

		Fixed::Fixed(void) : _fixed_point(0) {
	std::cout << "Default constructor called" << std::endl;
}

		Fixed::Fixed(int const i) : _fixed_point(i << this->_fract_bits) {
	std::cout << "Int constructor called\n";
}

		Fixed::Fixed(float const f) : _fixed_point(roundf(f * (1 << this->_fract_bits))) {
	std::cout << "Float constructor called\n";
}

		Fixed::Fixed(Fixed const & src) {
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
}

		Fixed::~Fixed(void) {
	std::cout << "Destructor called" << std::endl;
}

Fixed& Fixed::operator=(Fixed const & rhs) {

	std::cout << "Assignation operator called" << std::endl;
	this->_fixed_point = rhs._fixed_point;
	return *this;
}

int		Fixed::getRawBits(void) const {

	std::cout << "getRawBits member function called" << std::endl;
	return this-> _fixed_point;
}

void	Fixed::setRawBits(int const raw) {

	this->_fixed_point = raw;
	return;
}

float	Fixed::toFloat(void) const {

	return float(this->_fixed_point) / (1 << this->_fract_bits);
}

int		Fixed::toInt(void) const {

	return this->_fixed_point / (1 << this->_fract_bits);
}

std::ostream&	operator<<(std::ostream & o, Fixed const & i) {

	o << i.toFloat();
	return o;
}

const int Fixed::_fract_bits = 8;
