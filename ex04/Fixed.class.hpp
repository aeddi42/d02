/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 12:04:46 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/08 16:19:36 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_CLASS_HPP
# define FIXED_CLASS_HPP

#include <iostream>

class Fixed {

public:

					Fixed(void);
					Fixed(Fixed const & src);
					Fixed(int const i);
					Fixed(float const f);
					~Fixed(void);

	Fixed&			operator=(Fixed const & rhs);

	bool			operator>(Fixed const & rhs);
	bool			operator<(Fixed const & rhs);
	bool			operator>=(Fixed const & rhs);
	bool			operator<=(Fixed const & rhs);
	bool			operator==(Fixed const & rhs);
	bool			operator!=(Fixed const & rhs);

	Fixed			operator+(Fixed const & rhs);
	Fixed			operator-(Fixed const & rhs);
	Fixed			operator*(Fixed const & rhs);
	Fixed			operator/(Fixed const & rhs);

	Fixed			operator++(int);
	Fixed&			operator++(void);
	Fixed			operator--(int);
	Fixed&			operator--(void);

	int				getRawBits(void) const;
	void			setRawBits(int const raw);
	float			toFloat(void) const;
	int				toInt(void) const;

	static Fixed const &	max(Fixed const & a, Fixed const & b);
	static Fixed const &	min(Fixed const & a, Fixed const & b);

	static Fixed&	max(Fixed & a, Fixed & b);
	static Fixed&	min(Fixed & a, Fixed & b);

private:

	int					_fixed_point;
	static const int	_fract_bits;

};

std::ostream&	operator<<(std::ostream & o, Fixed const & i);

#endif /* !FIXED_CLASS_HPP */
