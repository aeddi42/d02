/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/08 22:20:09 by plastic           #+#    #+#             */
/*   Updated: 2015/04/09 10:53:51 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <sstream>
#include <stdlib.h>
#include "Fixed.class.hpp"

int	close_par = 0;
Fixed expression(std::istringstream & expr);

void	skipSpace(std::istringstream & expr)
{
	while(expr.peek() == ' ')
		expr.get();
}

Fixed number(std::istringstream & expr)
{
	std::string	lex;
	bool		first = false;
	Fixed		ret;

	while((expr.peek() >= '0' && expr.peek() <= '9') || expr.peek() == '.')
	{
		if (expr.peek() == '.')
		{
			if (first)
			{
				std::cerr << "Error with lexem: " << lex;
				expr >> lex;
				std::cerr << lex << std::endl;
				exit(1);
			}
			else
				first = true;
		}
		lex += expr.get();
	}
	ret = Fixed((float)atof(lex.c_str()));
	return ret;
}

Fixed factor(std::istringstream & expr)
{
	skipSpace(expr);
    if (expr.peek() >= '0' && expr.peek() <= '9')
        return number(expr);
    else if (expr.peek() == '(')
	{
		close_par++;
		expr.get();
		Fixed result = expression(expr);
		if (expr.peek() == ')')
		{
			close_par--;
			expr.get();
		}
		return result;
	}
	else if (expr.peek() == '-')
	{
		expr.get();
		Fixed ret = factor(expr) * Fixed(-1);
		return ret;
	}
	std::cerr << "Parsing error" << std::endl;
	exit(1);
}

Fixed term(std::istringstream & expr)
{
    Fixed result = factor(expr);
	Fixed ret;

	skipSpace(expr);
    if (expr.peek() != '*' && expr.peek() != '/')
		return result;
    while (expr.peek() == '*' || expr.peek() == '/')
	{
		if (expr.get() == '*')
			ret = result * factor(expr);
		else
			ret = result / factor(expr);
		skipSpace(expr);
	}
    return ret;
}

Fixed expression(std::istringstream & expr)
{
    Fixed result = term(expr);
	Fixed ret;

	skipSpace(expr);
    if (expr.peek() != '+' && expr.peek() != '-')
		return result;
    while (expr.peek() == '+' || expr.peek() == '-')
	{
        if (expr.get() == '+')
            ret = result + term(expr);
        else
            ret = result - term(expr);
		skipSpace(expr);
	}
    return ret;
}

int main(int ac, char **av)
{
	std::istringstream	expr;
	Fixed 				result;

	if (ac != 2)
	{
		std::cout << "Usage: " << av[0] << " \"( X + Y ) * Z\"" << std::endl;
		return 1;
	}
	expr.str(std::string(av[1]));
	skipSpace(expr);
	result = expression(expr);
	if (!expr.eof())
	{
		std::cerr << "Parsing error" << std::endl;
		exit(1);
	}
	else if (close_par)
	{
		std::cerr << "Missing close parenthesis" << std::endl;
		exit(1);
	}
	std::cout << result << std::endl;
    return 0;
}
