/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 12:04:58 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/08 16:22:24 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.class.hpp"
#include <iostream>
#include <cmath>

		Fixed::Fixed(void) : _fixed_point(0) {}

		Fixed::Fixed(int const i) : _fixed_point(i << this->_fract_bits) {}

		Fixed::Fixed(float const f) : _fixed_point(roundf(f * (1 << this->_fract_bits))) {}

		Fixed::Fixed(Fixed const & src) {
	*this = src;
}

		Fixed::~Fixed(void) {}

Fixed& Fixed::operator=(Fixed const & rhs) {

	this->_fixed_point = rhs._fixed_point;
	return *this;
}

bool	Fixed::operator>(Fixed const & rhs) {

	return (this->_fixed_point > rhs._fixed_point);
}

bool	Fixed::operator<(Fixed const & rhs) {

	return (this->_fixed_point < rhs._fixed_point);
}

bool	Fixed::operator>=(Fixed const & rhs) {

	return (this->_fixed_point >= rhs._fixed_point);
}

bool	Fixed::operator<=(Fixed const & rhs) {

	return (this->_fixed_point <= rhs._fixed_point);
}

bool	Fixed::operator==(Fixed const & rhs) {

	return (this->_fixed_point == rhs._fixed_point);
}

bool	Fixed::operator!=(Fixed const & rhs) {

	return (this->_fixed_point != rhs._fixed_point);
}

Fixed	Fixed::operator+(Fixed const & rhs) {

	float	tmp;

	tmp = this->toFloat() + rhs.toFloat();
	return Fixed(tmp);
}

Fixed	Fixed::operator-(Fixed const & rhs) {

	float	tmp;

	tmp = this->toFloat() - rhs.toFloat();
	return Fixed(tmp);
}

Fixed	Fixed::operator*(Fixed const & rhs) {

	float	tmp;

	tmp = this->toFloat() * rhs.toFloat();
	return Fixed(tmp);
}

Fixed	Fixed::operator/(Fixed const & rhs) {

	float	tmp;

	tmp = this->toFloat() / rhs.toFloat();
	return Fixed(tmp);
}

Fixed	Fixed::operator++(int) {

	Fixed	tmp;

	tmp = *this;
	this->_fixed_point++;
	return tmp;
}

Fixed&	Fixed::operator++(void) {

	this->_fixed_point++;
	return *this;
}

Fixed	Fixed::operator--(int) {

	Fixed	tmp;

	tmp = *this;
	this->_fixed_point--;
	return tmp;
}

Fixed&	Fixed::operator--(void) {

	this->_fixed_point--;
	return *this;
}

int		Fixed::getRawBits(void) const {

	return this->_fixed_point;
}

void	Fixed::setRawBits(int const raw) {

	this->_fixed_point = raw;
	return;
}

float	Fixed::toFloat(void) const {

	return float(this->_fixed_point) / (1 << this->_fract_bits);
}

int		Fixed::toInt(void) const {

	return this->_fixed_point / (1 << this->_fract_bits);
}

Fixed const &	Fixed::max(Fixed const & a, Fixed const & b) {

	if (a._fixed_point > b._fixed_point)
		return a;
	else
		return b;
}

Fixed const &	Fixed::min(Fixed const & a, Fixed const & b) {

	if (a._fixed_point < b._fixed_point)
		return a;
	else
		return b;
}

Fixed&	Fixed::max(Fixed & a, Fixed & b) {

	if (a._fixed_point > b._fixed_point)
		return a;
	else
		return b;
}

Fixed&	Fixed::min(Fixed & a, Fixed & b) {

	if (a._fixed_point < b._fixed_point)
		return a;
	else
		return b;
}


std::ostream&	operator<<(std::ostream & o, Fixed const & i) {

	o << i.toFloat();
	return o;
}

const int Fixed::_fract_bits = 8;
